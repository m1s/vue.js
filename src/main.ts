import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import clickOutside from './directives/v-click-outside';

const app = createApp(App);
const pinia = createPinia();

app.directive('click-outside', clickOutside);
app.use(pinia);
app.mount('#app');