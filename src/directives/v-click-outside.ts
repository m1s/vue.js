import { DirectiveBinding } from 'vue';

interface HTMLElementWithHandler extends HTMLElement {
  _clickOutsideHandler?: (event: MouseEvent) => void;
}

const clickOutside = {
  mounted(el: HTMLElementWithHandler, binding: DirectiveBinding) {
    const clickOutsideHandler = (event: MouseEvent) => {
      if (!el.contains(event.target as Node) && typeof binding.value === 'function') {
        const contextMenu = el.querySelector('.context-menu');
        if (contextMenu && !contextMenu.contains(event.target as Node)) {
          console.log('Click outside detected');
          binding.value(event);
        }
      }
    };

    document.addEventListener('click', clickOutsideHandler);

    el._clickOutsideHandler = clickOutsideHandler;
  },
  unmounted(el: HTMLElementWithHandler) {
    if (el._clickOutsideHandler) {
      document.removeEventListener('click', el._clickOutsideHandler);
      delete el._clickOutsideHandler;
    }
  },
};

export default clickOutside;