import { defineStore } from 'pinia';
import { monthTemplate } from './monthTemplate';

export const useStore = defineStore('main', {
  state: () => ({
    months: [...monthTemplate],
  }),
  actions: {
    toggleMonth(monthIndex: number) {
      this.months[monthIndex].selected = !this.months[monthIndex].selected;
    },
    selectAll(value: boolean) {
      this.months.forEach(month => month.selected = value);
    },
  },
  getters: {
    selectedMonthNames: (state) => {
      return state.months
        .filter(month => month.selected)
        .map(month => month.name);
    },
    selectedMonths: (state) => {
      return state.months.map(month => month.selected);
    },
  },
});