export const monthTemplate = [
    { name: 'Январь', selected: true },
    { name: 'Февраль', selected: true },
    { name: 'Март', selected: true },
    { name: 'Апрель', selected: false },
    { name: 'Май', selected: false },
    { name: 'Июнь', selected: false },
    { name: 'Июль', selected: false },
    { name: 'Август', selected: false },
    { name: 'Сентябрь', selected: false },
    { name: 'Октябрь', selected: false },
    { name: 'Ноябрь', selected: false },
    { name: 'Декабрь', selected: false },
];